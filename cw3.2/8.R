install.packages("data.tree")
install.packages("ahp")
library(ahp)
library(data.tree)

monitoryAhp <- Load("monitory.ahp")
Calculate(monitoryAhp)
Visualize(monitoryAhp)
AnalyzeTable(monitoryAhp, decisionMaker = "matka")
